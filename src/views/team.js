import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import TeamS1 from "../components/team-s1";
import TeamS2 from "../components/team-s2";

class Team extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <TeamS1/>
                <TeamS2/>
            </Container>    
        );
    }
}

export default Team;