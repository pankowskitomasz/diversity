import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import DiversityS1 from "../components/diversity-s1";
import DiversityS2 from "../components/diversity-s2";
import DiversityS3 from "../components/diversity-s3";

class Diversity extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <DiversityS1/>
                <DiversityS2/>
                <DiversityS3/>
            </Container>    
        );
    }
}

export default Diversity;